package com.company;

import java.util.Arrays;

public class Main {
//Question One
    public static String stringAddition(String num1, String num2) {
        StringBuilder sb = new StringBuilder();
        int carry = 0;
        int i = num1.length() - 1;
        int j = num2.length() - 1;
        while (i > -1 || j > -1) {
            int add = carry + (i < 0 ? 0 : num1.charAt(i--) - 48);
            add += j < 0 ? 0 : num2.charAt(j--) - 48;
            sb.append(add % 10);
            carry = add / 10;
        }
        return sb.append(carry == 1 ? "1" : "").reverse().toString();
    }

//Question Two

public static String tennisPlayers(int [] firstPlayerScores, int [] secondPlayerScores){

    int numberOfRounds=5;
    int [] result = new int[2];
    int playerOnePoint=0;
    int playerTwoPoint=0;

    for(int i = 0; i<numberOfRounds; i++){
        if(firstPlayerScores[i]==secondPlayerScores[i]){
            System.out.println("No points");
        }else if(firstPlayerScores[i] > secondPlayerScores[i]){
            playerOnePoint++;
        }else{
            playerTwoPoint++;
        }
    }

    result[0] = playerOnePoint;
    result[1] = playerTwoPoint;
    return Arrays.toString(result);
}

    public static void main(String[] args) {
        // write your code here
//Question One Test
        String a = "12";
        String b = "10";
        System.out.println(stringAddition(a, b));

//Question Two Test
        int [] TomScores = {1,4,7,2,4};
        int [] JackScores = {3,4,2,4,4};
        System.out.println(tennisPlayers(TomScores, JackScores));
    }
}




